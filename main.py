import os

video_src = '/home/db/VIDEO/long.mp4'
audio_src = 'http://localhost:80/'
output = r'hls -hls_flags delete_segments -hls_segment_filename /var/www/localhost/htdocs/tmp/seg-%03d.ts /var/www/localhost/htdocs/tmp/playlist.m3u8'

os.system(r'ffmpeg -stream_loop -1 -re -i '+ video_src +
	r' -i '+ audio_src +
	r' -c:v copy -c:a aac -strict experimental -map 0:v:0 -map 1:a:0 -f ' 
	+ output)
#script to start ffmpeg with our parameters